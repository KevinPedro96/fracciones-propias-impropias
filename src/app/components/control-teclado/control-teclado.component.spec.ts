import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTecladoComponent } from './control-teclado.component';

describe('ControlTecladoComponent', () => {
  let component: ControlTecladoComponent;
  let fixture: ComponentFixture<ControlTecladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTecladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTecladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
