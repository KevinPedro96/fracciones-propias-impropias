import { Component, OnInit } from '@angular/core';

import Phaser from "phaser";

let forma_2;
let forma_3;
let forma_4;
let forma_5;
let forma_6;
let forma_7;
let forma_8;
let forma_9;
let forma_10;
let enviar;


class myScene extends Phaser.Scene {


  constructor() {
    super({ key: 'main' })
  }

  preload() {

    this.load.image("Pie_2", "./assets/Forma_2.png")
    this.load.image("Pie_3", "./assets/Forma_3.png")
    this.load.image("Pie_4", "./assets/Forma_4.png")
    this.load.image("Pie_5", "./assets/Forma_5.png")
    this.load.image("Pie_6", "./assets/Forma_6.png")
    this.load.image("Pie_7", "./assets/Forma_7.png")
    this.load.image("Pie_8", "./assets/Forma_8.png")
    this.load.image("Pie_9", "./assets/Forma_9.png")
    this.load.image("Pie_10", "./assets/Forma_10.png")
    this.load.image("enviar", "./assets/Enviar.png")

  }


  create() {

    const denominador = {
      x: 100,
      y: 110,
      text:  9/* this.generadorRandom(2, 9)  */
    };

    const line = {
      x: 95,
      y: 83,
      text: '__'
    }

    const numerador = {
      x: 100,
      y: 80,
      text: 0,
     
    };

    this.add.text(denominador.x, denominador.y, [String(denominador.text)] ,{ font: '25px arial', color: '#FFFFFF' });
    this.add.text(line.x, line.y, line.text, { font: '25px arial', color: '#FFFFFF' });




    let xPos = 300;
    let yPos = 300;
    let angulo = 0;
    let tintRandom: any;
    let contador = 0;

    tintRandom = Math.random() * 16000000;


    if (denominador.text === 2) {

      numerador.text = 3;
      this.add.text(numerador.x, numerador.y, [String(numerador.text)],{ font: '25px arial', color: '#FFFFFF' });


      for (let a = 0; a < 2; a++) {
        forma_2 = this.add.image(xPos, yPos, "Pie_2").setInteractive({ pixelPerfect: true })
        forma_2.setScale(0.200);
        forma_2.setOrigin(1, 0.5);
        forma_2.setAngle(angulo);
        angulo = angulo + 180;
        /* forma_2.setTint(tintRandom); */

        forma_2.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })
      }

      for (let a = 0; a < 2; a++) {
        forma_2 = this.add.image(xPos + 400, yPos, "Pie_2").setInteractive({ pixelPerfect: true })
        forma_2.setScale(0.200);
        forma_2.setOrigin(1, 0.5);
        forma_2.setAngle(angulo);
        angulo = angulo + 180;
        /* forma_2.setTint(tintRandom); */

        forma_2.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })
      }
     
      

    } else if (denominador.text === 3) {

      numerador.text = this.generadorRandom(4, 5)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let b = 0; b < 3; b++) {
        forma_3 = this.add.image(xPos, yPos, "Pie_3").setInteractive({ pixelPerfect: true });
        forma_3.setScale(0.200);
        forma_3.setOrigin(1, 0.662);
        forma_3.setAngle(angulo);
        angulo = angulo + 120;
        // forma_3.setTint(tintRandom);

        forma_3.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let b = 0; b < 3; b++) {
        forma_3 = this.add.image(xPos + 400, yPos, "Pie_3").setInteractive({ pixelPerfect: true });
        forma_3.setScale(0.200);
        forma_3.setOrigin(1, 0.662);
        forma_3.setAngle(angulo);
        angulo = angulo + 120;
        // forma_3.setTint(tintRandom);

        forma_3.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } else if (denominador.text === 4) {

      numerador.text = this.generadorRandom(5, 7)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let b = 0; b < 4; b++) {
        forma_4 = this.add.image(xPos, yPos, "Pie_4").setInteractive({ pixelPerfect: true });
        forma_4.setScale(0.200);
        forma_4.setOrigin(1, 1);
        forma_4.setAngle(angulo);
        angulo = angulo + 90;
        // forma_4.setTint(tintRandom);

        forma_4.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let b = 0; b < 4; b++) {
        forma_4 = this.add.image(xPos + 400, yPos, "Pie_4").setInteractive({ pixelPerfect: true });
        forma_4.setScale(0.200);
        forma_4.setOrigin(1, 1);
        forma_4.setAngle(angulo);
        angulo = angulo + 90;
        // forma_4.setTint(tintRandom);

        forma_4.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } else if (denominador.text === 5) {

      numerador.text = this.generadorRandom(6, 9)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let b = 0; b < 5; b++) {
        forma_5 = this.add.image(xPos, yPos, "Pie_5").setInteractive({ pixelPerfect: true });
        forma_5.setScale(0.100);
        forma_5.setOrigin(1, 1);
        forma_5.setAngle(angulo);
        angulo = angulo + 72;
        // forma_5.setTint(tintRandom);

        forma_5.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let b = 0; b < 5; b++) {
        forma_5 = this.add.image(xPos + 400, yPos, "Pie_5").setInteractive({ pixelPerfect: true });
        forma_5.setScale(0.100);
        forma_5.setOrigin(1, 1);
        forma_5.setAngle(angulo);
        angulo = angulo + 72;
        // forma_5.setTint(tintRandom);

        forma_5.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } else if (denominador.text === 6) {

      numerador.text = this.generadorRandom(7, 11)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let b = 0; b < 6; b++) {
        forma_6 = this.add.image(xPos, yPos, "Pie_6").setInteractive({ pixelPerfect: true });
        forma_6.setScale(0.080);
        forma_6.setOrigin(1, 1);
        forma_6.setAngle(angulo);
        angulo = angulo + 60;
        // forma_6.setTint(tintRandom);

        forma_6.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let b = 0; b < 6; b++) {
        forma_6 = this.add.image(xPos + 400, yPos, "Pie_6").setInteractive({ pixelPerfect: true });
        forma_6.setScale(0.080);
        forma_6.setOrigin(1, 1);
        forma_6.setAngle(angulo);
        angulo = angulo + 60;
        // forma_6.setTint(tintRandom);

        forma_6.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } else if (denominador.text === 7) {

      numerador.text = this.generadorRandom(8, 13)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });

      for (let b = 0; b < 7; b++) {
        forma_7 = this.add.image(xPos, yPos, "Pie_7").setInteractive({ pixelPerfect: true });
        forma_7.setScale(0.080);
        forma_7.setOrigin(1, 1);
        forma_7.setAngle(angulo);
        angulo = angulo + 51.5;
        // forma_7.setTint(tintRandom);

        forma_7.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })


      }

      for (let b = 0; b < 7; b++) {
        forma_7 = this.add.image(xPos + 400, yPos, "Pie_7").setInteractive({ pixelPerfect: true });
        forma_7.setScale(0.080);
        forma_7.setOrigin(1, 1);
        forma_7.setAngle(angulo);
        angulo = angulo + 51.5;
        // forma_7.setTint(tintRandom);

        forma_7.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })


      }

    } else if (denominador.text === 8) {

      numerador.text = this.generadorRandom(9, 15)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let i = 0; i < 8; i++) {
        forma_8 = this.add.image(xPos, yPos, "Pie_8").setInteractive({ pixelPerfect: true })
        forma_8.setScale(0.100);
        forma_8.setOrigin(0, 1);
        forma_8.setAngle(angulo)
        angulo = angulo + 45
        // forma_8.setTint(tintRandom)

        forma_8.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let i = 0; i < 8; i++) {
        forma_8 = this.add.image(xPos + 400, yPos, "Pie_8").setInteractive({ pixelPerfect: true })
        forma_8.setScale(0.100);
        forma_8.setOrigin(0, 1);
        forma_8.setAngle(angulo)
        angulo = angulo + 45
        // forma_8.setTint(tintRandom)

        forma_8.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } else if (denominador.text === 9) {

      numerador.text = this.generadorRandom(10, 17)
      this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });


      for (let i = 0; i < 9; i++) {
        forma_9 = this.add.image(xPos, yPos, "Pie_9").setInteractive({ pixelPerfect: true })
        forma_9.setScale(0.070);
        forma_9.setOrigin(0, 1);
        forma_9.setAngle(angulo)
        angulo = angulo + 40
        // forma_9.setTint(tintRandom)

        forma_9.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

      for (let i = 0; i < 9; i++) {
        forma_9 = this.add.image(xPos + 400, yPos, "Pie_9").setInteractive({ pixelPerfect: true })
        forma_9.setScale(0.070);
        forma_9.setOrigin(0, 1);
        forma_9.setAngle(angulo)
        angulo = angulo + 40
        // forma_9.setTint(tintRandom)

        forma_9.on('pointerdown', function () {
          if (this.isTinted) {
            this.clearTint();
            contador--
          }
          else {
            this.setTint(tintRandom);
            contador++
          }

        })

      }

    } /* else if (denominador.text === 10) {

      for(let i = 0; i < 10; i ++)
      {
        forma_10 = this.add.image(xPos,yPos,"Pie_10").setInteractive({ pixelPerfect: true })
        forma_10.setScale(0.070);
        forma_10.setOrigin(1,1);
        forma_10.setAngle(angulo)
        angulo = angulo +40
        forma_10.setTint(tintRandom)

        forma_10.on('pointerdown', function (){
          if (this.isTinted)
          {
            this.clearTint();
          contador ++          }
          else 
          {
            this.setTint(tintRandom);
            contador --
          }
          
        })

      }  

      for(let i = 0; i < 10; i ++)
      {
        forma_10 = this.add.image(xPos + 600,yPos,"Pie_10").setInteractive({ pixelPerfect: true })
        forma_10.setScale(0.070);
        forma_10.setOrigin(1,1);
        forma_10.setAngle(angulo)
        angulo = angulo +40
        forma_10.setTint(tintRandom)

        forma_10.on('pointerdown', function (){
          if (this.isTinted)
          {
            this.clearTint();
          contador ++          }
          else 
          {
            this.setTint(tintRandom);
            contador --
          }
          
        })

      }  
    }  */

    /*   
    */
    enviar = this.add.image(1000, 500, "enviar").setInteractive();
    enviar.setScale(0.400)
    enviar.setTint(0xEED715)

    enviar.on('pointerdown', function () {
      console.log(contador);

      if(numerador.text === contador){
        console.log('La fraccion es correcta');
  
  
      }
      else {
        console.log('La fraccion es incorrecta');
  
      }

    })






  }

  generadorRandom(min, max) {

    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (1 + max - min) + min)

  }

  update() {
    /* forma_3.angle++; */
  }

}


@Component({
  selector: 'app-control-teclado',
  templateUrl: './control-teclado.component.html',
  styleUrls: ['./control-teclado.component.css']
})
export class ControlTecladoComponent implements OnInit {


  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;

  constructor() {
    this.config = {
      type: Phaser.AUTO,
      width: 1200,
      height: 600,
      backgroundColor: '#2d2d2d',
      parent: 'phaser-example',
      scene: [myScene],
      physics: {
        default: "arcade",
        arcade: {
          gravity: {
            y: 500
          }
        }
      }
    }
  }

  ngOnInit(): void {

    this.phaserGame = new Phaser.Game(this.config);
  }

}
