import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuegoNuevoComponent } from './juego-nuevo.component';

describe('JuegoNuevoComponent', () => {
  let component: JuegoNuevoComponent;
  let fixture: ComponentFixture<JuegoNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuegoNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuegoNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
