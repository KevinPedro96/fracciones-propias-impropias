import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoNuevoComponent } from './nuevo-nuevo.component';

describe('NuevoNuevoComponent', () => {
  let component: NuevoNuevoComponent;
  let fixture: ComponentFixture<NuevoNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
