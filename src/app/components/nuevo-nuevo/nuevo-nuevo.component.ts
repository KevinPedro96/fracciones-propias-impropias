import { Component, OnInit } from '@angular/core';

import Phaser from "phaser";

let enviar;
var data;

class myScene extends Phaser.Scene {

  imagen1 = [];


  constructor() {
    super({ key: 'main' })
  }

  preload() {
    this.load.image("manzana", "./assets/Dibujo" + this.generadorRandom(0, 6) + ".png")
    this.load.image("enviar", "./assets/Enviar.png")
  }

  create() {





    const denominador = {
      x: 100,
      y: 110,
      text: this.generadorRandom(4, 10)

    }

    const numerador = {
      x: 100,
      y: 80,
      text: this.generadorRandom(1, denominador.text - 1)

    }

    data = {
      numerador: numerador,
      denominador: denominador,
      matriz: [],
      respuesta: 0
    }



    const line = {
      x: 95,
      y: 83,
      text: "__"

    }

    this.add.text(denominador.x, denominador.y, [String(denominador.text)], { font: '25px arial', color: '#FFFFFF' });
    this.add.text(line.x, line.y, line.text, { font: '25px arial', color: '#FFFFFF' });
    this.add.text(numerador.x, numerador.y, [String(numerador.text)], { font: '25px arial', color: '#FFFFFF' });



    let xpos = 100;
    let ypos = 300;
    let contador = 0;

    // comoguardar los resultados y mostrarlos


    for (let i = 0; i <= denominador.text - 1; i++) {
      // imagen1 = this.add.image(xpos, ypos, 'manzana').setInteractive({ pixelPerfect: true });
      this.imagen1.push(this.add.image(xpos, ypos, 'manzana').setInteractive({ pixelPerfect: true }));
      console.log(this.imagen1[i], '/', this.imagen1);

      this.imagen1[i].setTint(0x3F3F3F)
      this.imagen1[i].setScale(0.3)



      xpos = xpos + 110;

      this.imagen1[i].on('pointerdown', function () {
        console.log(i);

        if (this.isTinted) {
          this.clearTint();
          contador++;
          data.matriz.push(i);


        }
        else {
          this.setTint(0x3F3F3F);
          contador--;
          let pos = data.matriz.indexOf(i);
          data.matriz.splice(pos, 1);
        }

      })

    }

    enviar = this.add.image(1000, 500, "enviar").setInteractive();
    enviar.setScale(0.400)
    enviar.setTint(0xEED715)

    enviar.on('pointerdown', function () {


      if (numerador.text === contador) {
        console.log('La fraccion es correcta');
        // localStorage.setItem('resultadoFraccion', String(contador));
        data.respuesta = contador;
        console.log(data);


      }
      else {
        console.log('La fraccion es incorrecta');

      }

    })





  }
  generadorRandom(min, max) {

    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (1 + max - min) + min)
  }

  update() {




  }
}

@Component({
  selector: 'app-nuevo-nuevo',
  templateUrl: './nuevo-nuevo.component.html',
  styleUrls: ['./nuevo-nuevo.component.css']
})
export class NuevoNuevoComponent implements OnInit {

  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;

  constructor() {
    this.config = {
      type: Phaser.AUTO,
      width: 1200,
      height: 600,
      backgroundColor: '#2d2d2d',
      parent: 'phaser-example',
      scene: [myScene],
      physics: {
        default: "arcade",

      }
    }
  }

  ngOnInit(): void {

    this.phaserGame = new Phaser.Game(this.config);
  }

}
